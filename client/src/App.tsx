import React, { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import './App.css';

interface AppProps {
  children: ReactNode;
}

// Делаем navbar с ссылками на компоненты

const App: React.FC<AppProps> = ({ children }) => (
  <div className="App">
    <ul className="menu">
      <li><Link to="/">Главная</Link></li>
      <li><Link to="/arachnoentomology">Арахноэнтомология</Link></li>
      <li><Link to="/helminthology">Гельминтология</Link></li>
      <li><Link to="/protozoology">Протозоология</Link></li>
    </ul>
    {children}
  </div>
);

export default App;