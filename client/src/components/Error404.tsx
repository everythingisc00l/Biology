// Компонент отображает страницу ошибки 404

const Error404 = () => (
  <div className="Error404">
   <h1>Ошибка 404. Страница не найдена.</h1>
  </div>
  )
  export default Error404