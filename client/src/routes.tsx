import { Routes, Route } from 'react-router-dom'

// Импорт компонентов
import App from './App'
import Home from './components/Home'
import Arachnoentomology from './components/Arachnoentomology'
import Helminthology from './components/Helminthology'
import Protozoology from './components/Protozoology'
import Error404 from './components/Error404'

// Прописываем пути url к компонентам
const AppRoutes = () => (
    <App>
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/arachnoentomology" element={<Arachnoentomology />} />
            <Route path="/helminthology" element={<Helminthology />} />
            <Route path="/protozoology" element={<Protozoology />} />
            <Route path=":arachnoentomologyId" element={<Arachnoentomology />} />
            <Route path=":helminthologyId" element={<Helminthology />} />
            <Route path=":protozoologyId" element={<Protozoology />} />
            <Route path="*" element={<Error404 />} />
        </Routes>
    </App>
)

export default AppRoutes